//
//  Scene.h
//  Game
//
//  Created by Admin on 16.07.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import "GameOver.h"

/// Main scene, shows meteorits and rocket, processes them.
@interface Scene : SKScene <SKPhysicsContactDelegate>
{
    BOOL createScene;
    SKSpriteNode * Rock;
    SKSpriteNode * Rock2;
    SKSpriteNode * Rock3;
    SKSpriteNode * hull;
    SKSpriteNode * Earth;
    SKLabelNode * HitPoint;
    SKLabelNode * scoreLable;
    int Score;
    int digit1;
    int digit2;
    int digit3;
    int digit4;
    int digit5;
    int digit6;

    int CounterEROR, i, hp;
    int RockPosition_Y;
    int RockPosition_X;
}

@property (strong, nonatomic) NSMutableArray * DigitArray;

@end
