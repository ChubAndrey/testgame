//
//  ViewController.m
//  Game
//
//  Created by Admin on 16.07.14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

//=========================================================================

- (void)viewDidLoad
{
    [super viewDidLoad];
    SKView * skView = (SKView *)self.view;
    skView.showsFPS = YES;
    skView.showsNodeCount = YES;
    
    startScene *start = [startScene sceneWithSize:skView.bounds.size];
    start.scaleMode = SKSceneScaleModeAspectFill;
    [skView presentScene:start];
//    GameOver *start = [GameOver sceneWithSize:skView.bounds.size];
//    start.scaleMode = SKSceneScaleModeAspectFill;
//    [skView presentScene:start];
}

//=========================================================================

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//=========================================================================

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

//=========================================================================


@end
