//
//  startScene.m
//  Game
//
//  Created by Admin on 22.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "startScene.h"

@implementation startScene
#define DEGREES_RADIANS(angle) ((angle) / 180.0 * M_PI)
-(void) didMoveToView:(SKView *)view
{
    if(!createScene)
    {
        [self removeAllActions];
        [self removeAllChildren];
        [self createBackground];
        cabin = [self cabinNode];
        TextNode = [self createText];
        [self addChild:cabin];
        [self addChild:TextNode];
    }
}

//=========================================================================

static inline CGFloat skRandf() {
    return rand() / (CGFloat) RAND_MAX;
}
static inline CGFloat skRand(CGFloat low, CGFloat high) {
    return skRandf() * (high - low) + low;
}

//=========================================================================

-(SKSpriteNode*) cabinNode
{
    SKSpriteNode * _cabin = [SKSpriteNode spriteNodeWithImageNamed:@"cabin2"];
    _cabin.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    
    SKSpriteNode *button = [SKSpriteNode spriteNodeWithImageNamed:@"UC9AxBaecvg"];
    button.yScale = 0.5;
    button.xScale = 0.5;
    button.name = @"button";
    button.position = CGPointMake(10, -125);
    SKAction *pulseRed = [SKAction sequence:@[
                                              [SKAction colorizeWithColor:[SKColor redColor] colorBlendFactor:0.9 duration:0.15],
                                              [SKAction waitForDuration:0.3],
                                              [SKAction colorizeWithColorBlendFactor:0.0 duration:0.15]]];
    
    [button runAction: [SKAction repeatActionForever:pulseRed]];
    [_cabin addChild:button];
    return _cabin;
}

//=========================================================================


- (void) newLight
{
    SKSpriteNode *light = [[SKSpriteNode alloc] initWithColor:[SKColor whiteColor]
                                                        size:CGSizeMake(8,8)];
    light.name = @"light";
    light.position = CGPointMake(skRand(0, self.size.width), skRand(260, 540));
    SKAction *blink = [SKAction sequence:@[
                                           [SKAction fadeOutWithDuration:0.25],
                                           [SKAction fadeInWithDuration:0.25]]];
    SKAction *blinkForever = [SKAction repeatActionForever:blink];
    [light runAction: blinkForever];
    [self addChild:light];
}

//=========================================================================

- (SKSpriteNode*) newLight2
{
    SKSpriteNode *light = [[SKSpriteNode alloc] initWithColor:[SKColor whiteColor]
                                                         size:CGSizeMake(3,3)];
    light.name = @"light2";
    light.position = CGPointMake(skRand(0, self.size.width), skRand(260, 540));
//    SKAction *blink = [SKAction sequence:@[
//                                           [SKAction fadeOutWithDuration:0.25],
//                                           [SKAction fadeInWithDuration:0.25]]];
//    SKAction *blinkForever = [SKAction repeatActionForever:blink];
//    [light runAction: blinkForever];
    return light;
}
//=========================================================================

-(void)createBackground
{
    bg = [SKSpriteNode spriteNodeWithColor:[SKColor blackColor]
                                                    size:CGSizeMake(320, 480)];
    bg.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    
    SKAction *makeLight = [SKAction sequence:@[[SKAction performSelector:@selector(newLight) onTarget:self],[SKAction waitForDuration:2 withRange:0.35]]];
    
    [bg runAction:[SKAction repeatAction:makeLight count:20]];
//    text = [self createLableText];
//    text.position = CGPointMake(0.0, 20);
//    text.text = [NSString stringWithFormat:@"HI Hero"];
////    text.position = CGPointMake(0.0, 20);
//    [bg addChild:text];
    [self addChild:bg];
}

//=========================================================================

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    
    if ([node.name isEqualToString:@"button"]) {
        NSLog(@"button up");
        SKAction *moveUp = [SKAction moveByX: 0 y: 20.0 duration: 0.5];
        SKAction *zoom = [SKAction scaleTo: 0 duration: 0];
//        SKAction *rotation = [SKAction rotateByAngle: DEGREES_RADIANS(360) duration:0.5];
        SKAction *fadeAway = [SKAction fadeOutWithDuration: 1.0];
        SKAction *remove = [SKAction removeFromParent];
        
        SKAction *moveSequence = [SKAction sequence:@[moveUp, zoom, /*rotation,*/ fadeAway, remove]];
        [TextNode runAction: moveSequence completion:^{
            SKAction *wait = [SKAction fadeOutWithDuration:0.5];
            [TextNode runAction:wait];}];
        
        [self accelerationEffect];

    }
}

//=========================================================================

- (SKLabelNode*) createLableText
{
    SKLabelNode *text2 = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    text2.name = @"text";
    [text2 setFontColor:[SKColor yellowColor]];
    text2.fontSize = 20;
    return text2;
}

//=========================================================================

- (SKSpriteNode*) createText
{
    SKSpriteNode *Text = [SKSpriteNode spriteNodeWithColor:[SKColor blackColor]
                                                      size:CGSizeMake(100, 100)];
    Text.position = CGPointMake(CGRectGetMidX(self.frame)+9, CGRectGetMidY(self.frame)+50);
    
    SKLabelNode *text2 = [self createLableText];
    SKLabelNode *text3 = [self createLableText];
    SKLabelNode *text4 = [self createLableText];
    text2.position = CGPointMake(0.0, 20);
    text2.text = [NSString stringWithFormat:@"HI Hero"];
    text3.position = CGPointMake(0.0, 0.0);
    text3.text = [NSString stringWithFormat:@"push START"];
    text4.position = CGPointMake(0.0, -20);
    text4.text = [NSString stringWithFormat:@"and protect the planet"];
    [Text addChild:text2];
    [Text addChild:text3];
    [Text addChild:text4];
    return Text;
}

//=========================================================================

-(void) accelerationEffect
{
    
    [self enumerateChildNodesWithName:@"light" usingBlock:^(SKNode *node, BOOL *stop)
     {
             [node removeFromParent];
     }];
    
    SKAction *moveDown = [SKAction moveByX: -2 y: -5.0 duration: 0.5];
    SKAction *zoom = [SKAction scaleTo: 1.5 duration: 0.0];
    
    SKAction *moveDown1 = [SKAction moveByX: -7 y: -7.0 duration: 0.4];
    SKAction *zoom1 = [SKAction scaleTo: 2 duration: 0.0];
    
    SKAction *moveDown2 = [SKAction moveByX: -10 y: -10.0 duration: 0.2];
    SKAction *zoom2 = [SKAction scaleTo: 2.5 duration: 0.0];
    
    SKAction *moveDown3 = [SKAction moveByX: -11 y: -12.0 duration: 0.0];
    SKAction *zoom3 = [SKAction scaleTo: 2.9 duration: 0.0];
    
    SKAction *moveDown4 = [SKAction moveByX: -12 y: -14.0 duration: 0.0];
    SKAction *zoom4 = [SKAction scaleTo: 2.9 duration: 0.0];
    
    SKAction *rotation = [SKAction rotateByAngle: DEGREES_RADIANS(360) duration:0.0];
//    SKAction *fadeAway = [SKAction fadeOutWithDuration: 3.0];
    SKAction *remove = [SKAction removeFromParent];
    SKAction *moveSequence2 = [SKAction sequence:@[moveDown, zoom, rotation, moveDown1, zoom1, rotation, moveDown2, zoom2, rotation, moveDown3, zoom3, rotation, moveDown4, zoom4, remove]];
    for (int i=0; i<20; i++)
    {
        SKSpriteNode *light = [self newLight2];
        [light runAction: moveSequence2];
        [self addChild:light];
        
        SKAction *cabinINr = [SKAction moveByX:+i/12 y:0 duration:0.1];
        SKAction *cabinINl = [SKAction moveByX:-i/12 y:0 duration:0.1];
        SKAction *moveSequence3 = [SKAction sequence:@[cabinINr, cabinINl,cabinINr, cabinINl,cabinINr, cabinINl,cabinINr, cabinINl,cabinINr, cabinINl,cabinINr, cabinINl, cabinINr, cabinINl,cabinINr, cabinINl]];
        [cabin runAction:moveSequence3 completion:^{
            SKScene *scene = [[Scene alloc]initWithSize:self.size];
            SKTransition *doors = [SKTransition doorsOpenHorizontalWithDuration:0.2];
            [self.view presentScene:scene transition:doors];
            
        }];
        
    }
}

@end
