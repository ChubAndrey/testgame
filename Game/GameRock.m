//
//  GameRock.m
//  Game
//
//  Created by Admin on 01.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "GameRock.h"

@implementation GameRock

-(BOOL)isEqualToSum:(NSInteger)sum
{
    return sum == _firstValue+_secondValue;
}

@end
