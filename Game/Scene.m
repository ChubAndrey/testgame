//
//  Scene.m
//  Game
//
//  Created by Admin on 16.07.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "Scene.h"
#define DEGREES_RADIANS(angle) ((angle) / 180.0 * M_PI)
static const uint32_t hullCategory = 0x1 << 0;
static const uint32_t rockCategory = 0x1 << 1;
static const uint32_t planetCategory = 0x1 << 2;

@implementation Scene

//=========================================================================

-(NSMutableArray *) DigitArray {
    
    if(!_DigitArray)
        _DigitArray = [[NSMutableArray alloc] init];
    return _DigitArray;
}

//=========================================================================

-(int) skRand
{
    
    int digtRand = rand() %10;
    return digtRand;
}

//=========================================================================

-(void)didMoveToView:(SKView *)view
{

    self.physicsWorld.contactDelegate = self;

    if (!createScene)
    {
        hp = 100;
        [self removeAllActions];
        [self removeAllChildren];
        [self createBackground];
        [self scoreLable];
        [self hpEarth];
        [self createEarth];
        [self configureTwoRocks :1];
        createScene=YES;
    }
}

//=========================================================================

-(void)createBackground
{
    
    SKSpriteNode * bg = [SKSpriteNode spriteNodeWithImageNamed:@"7671"];
    bg.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    [self addChild:bg];
    
}

//=========================================================================

-(void)createFirstRock : (int) first : (int) second : (int) myWidht
{
    Rock = [SKSpriteNode spriteNodeWithImageNamed:@"Rock"];
    Rock.name = @"Rock";
    [Rock setSize:CGSizeMake(40, 40)];
    Rock.position = CGPointMake(myWidht, self.size.height);
    
    SKAction * dynamikRoc = [SKAction sequence:@[[SKAction moveByX:0.0 y:-400 duration:10]]];
    
    Rock.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:30];
    //Rock.physicsBody.usesPreciseCollisionDetection = NO;
    Rock.physicsBody.dynamic = YES;
    Rock.physicsBody.affectedByGravity = NO;
    Rock.physicsBody.categoryBitMask = rockCategory;
    Rock.physicsBody.contactTestBitMask = hullCategory | planetCategory;
    Rock.physicsBody.collisionBitMask = planetCategory;
    
    [Rock runAction:[SKAction repeatActionForever:dynamikRoc]];
    [self addChild:Rock];
    
    SKLabelNode * equation = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    equation.text= [NSString stringWithFormat:@"%d + %d",first, second];
    [equation setFontColor:[SKColor yellowColor]];
    equation.fontSize = 20;
    equation.position = CGPointMake(0.0, 0.0);
    [Rock addChild:equation];
}

//=========================================================================

-(void)createSecondRock : (int) first : (int) second : (int) myWidht
{
    Rock2 = [SKSpriteNode spriteNodeWithImageNamed:@"Rock"];
    Rock2.name = @"Rock";
    [Rock2 setSize:CGSizeMake(40, 40)];
    Rock2.position = CGPointMake(myWidht, self.size.height);
    
    SKAction * dynamikRoc = [SKAction sequence:@[[SKAction moveByX:0.0 y:-400 duration:10]]];
    
    Rock2.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:30];
    //Rock.physicsBody.usesPreciseCollisionDetection = NO;
    Rock2.physicsBody.dynamic = YES;
    Rock2.physicsBody.affectedByGravity = NO;
    Rock2.physicsBody.categoryBitMask = rockCategory;
    Rock2.physicsBody.contactTestBitMask = hullCategory | planetCategory;
    Rock2.physicsBody.collisionBitMask = planetCategory;
    
    [Rock2 runAction:[SKAction repeatActionForever:dynamikRoc]];
    [self addChild:Rock2];
    
    SKLabelNode * equation = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    equation.text= [NSString stringWithFormat:@"%d + %d",first, second];
    [equation setFontColor:[SKColor yellowColor]];
    equation.fontSize = 20;
    equation.position = CGPointMake(0.0, 0.0);
    [Rock2 addChild:equation];
}

//=========================================================================

-(void)createThirdRock : (int) first : (int) second : (float) myHeight
{
    Rock3 = [SKSpriteNode spriteNodeWithImageNamed:@"Rock"];
    Rock3.name = @"Rock";
    [Rock3 setSize:CGSizeMake(40, 40)];
    Rock3.position = CGPointMake(CGRectGetMidX (self.frame), myHeight);
    
    SKAction * dynamikRoc = [SKAction sequence:@[[SKAction moveByX:0.0 y:-400 duration:10]]];
    
    Rock3.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:30];
    //Rock.physicsBody.usesPreciseCollisionDetection = NO;
    Rock3.physicsBody.dynamic = YES;
    Rock3.physicsBody.affectedByGravity = NO;
    Rock3.physicsBody.categoryBitMask = rockCategory;
    Rock3.physicsBody.contactTestBitMask = hullCategory | planetCategory;
    Rock3.physicsBody.collisionBitMask = planetCategory;
    
    [Rock3 runAction:[SKAction repeatActionForever:dynamikRoc]];
    [self addChild:Rock3];
    
    SKLabelNode * equation = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    equation.text= [NSString stringWithFormat:@"%d + %d",first, second];
    [equation setFontColor:[SKColor yellowColor]];
    equation.fontSize = 20;
    equation.position = CGPointMake(0.0, 0.0);
    [Rock3 addChild:equation];
}

//=========================================================================

-(void)createHull : (int) sum {
    
    hull = [SKSpriteNode spriteNodeWithImageNamed:@"spaceship"];
    hull.name = @"hull";
    [hull setSize:CGSizeMake(51,90)];
    hull.position = CGPointMake(CGRectGetMidX (self.frame) , CGRectGetMidY(self.frame)-400);
    SKAction * moveToPosition = [SKAction sequence:@[[SKAction moveToY:50 duration:1]]];
    hull.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:hull.size];
    hull.physicsBody.dynamic = YES;
    hull.physicsBody.affectedByGravity = NO;
    
    hull.physicsBody.categoryBitMask = hullCategory;
    hull.physicsBody.contactTestBitMask = rockCategory;
    hull.physicsBody.collisionBitMask = rockCategory;
    
    SKLabelNode * SUM = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    NSLog(@"sum2 %d", sum);
    SUM.text = [NSString stringWithFormat:@"%d", sum];
    [SUM setFontColor:[SKColor blueColor]];
    SUM.fontSize = 20;
    SUM.position = CGPointMake(0.0, 0.0);
    [hull addChild:SUM];
    [hull runAction:moveToPosition];
    [self addChild:hull];
    
}

-(void) createEarth
{
    Earth = [SKSpriteNode spriteNodeWithImageNamed:@"EarthFromSpace"];
    Earth.name = @"Earth";
    Earth.size = CGSizeMake(320, 180);
    Earth.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)-200);
    Earth.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:Earth.size];
    Earth.physicsBody.dynamic = NO;
    Earth.physicsBody.affectedByGravity = NO;
    Earth.physicsBody.categoryBitMask = planetCategory;
    Earth.physicsBody.collisionBitMask = rockCategory;
    Earth.physicsBody.contactTestBitMask = rockCategory;
    
    [self addChild:Earth];
}

//=========================================================================

-(void) hpEarth
{
    HitPoint = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    HitPoint.text = [NSString stringWithFormat:@"hp Earth %d", hp];
    [HitPoint setFontColor:[SKColor greenColor]];
    HitPoint.fontSize = 20;
    HitPoint.position = CGPointMake( 300, self.size.height-550);
    [HitPoint setZPosition:1];
    [HitPoint setName:@"HP"];
    [HitPoint setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeRight];
    [self addChild:HitPoint];
}

//=========================================================================

-(void) scoreLable
{
    scoreLable = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    scoreLable.text = [NSString stringWithFormat:@"Score %d", Score];
    [scoreLable setFontColor:[SKColor yellowColor]];
    scoreLable.fontSize = 20;
    scoreLable.position = CGPointMake(10, self.size.height-550);
    [scoreLable setZPosition:1];
    [scoreLable setName:@"Score"];
    [scoreLable setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeLeft];
    [self addChild:scoreLable];
    
}

//=========================================================================

-(void)BOOM : (CGFloat) point1 : (CGFloat) point2 {
    
    SKSpriteNode * BooM = [SKSpriteNode spriteNodeWithImageNamed:@"31"];
    [BooM setSize:CGSizeMake(50, 50)];
    BooM.position = CGPointMake(point1, point2);
    SKAction *zoom = [SKAction scaleTo: 2.0 duration: 0.55];
    SKAction *rotation = [SKAction rotateByAngle: DEGREES_RADIANS(360) duration:0.5];
    SKAction *fadeAway = [SKAction fadeOutWithDuration: 1.0];
    SKAction *remove = [SKAction removeFromParent];
    [hull removeFromParent];
    [self enumerateChildNodesWithName:@"Rock" usingBlock:^(SKNode *node, BOOL *stop) {

            [node removeFromParent];
    }];
    SKAction *moveSequence = [SKAction sequence:@[zoom, rotation, fadeAway, remove]];
    [BooM runAction: moveSequence];
    [self addChild:BooM];
    NSLog(@"BOOOOOOOOMMMMMM");
}

//=========================================================================

-(void) angleOFrotationHull {
    
    if(RockPosition_X<CGRectGetMidX(self.frame)){
        hull.zRotation = M_PI * hull.physicsBody.velocity.dy *0.0001;
    }
    if(RockPosition_X<CGRectGetMidX(self.frame)-100){
        hull.zRotation = M_PI * hull.physicsBody.velocity.dy *0.0003;
    }
    if(RockPosition_X>CGRectGetMidX(self.frame)){
        hull.zRotation = M_PI * hull.physicsBody.velocity.dy *(-0.0001);
    }
    if(RockPosition_X>CGRectGetMidX(self.frame)+100){
        hull.zRotation = M_PI * hull.physicsBody.velocity.dy *(-0.0003);
    }
    
}

//=========================================================================

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    

    if ([node.name isEqualToString:@"Rock"]) {
        
        RockPosition_X = node.position.x;
        RockPosition_Y = node.position.y;
            [hull.physicsBody setVelocity:CGVectorMake(node.position.x-150, node.position.y)];
    }
}

//=========================================================================

-(void)update:(CFTimeInterval)currentTime
{
    [self angleOFrotationHull];
    
    if (hull.position.y > 480){
        [hull removeFromParent];
    }

}

//=========================================================================

-(void) didBeginContact:(SKPhysicsContact *)contact
{
    SKPhysicsBody * firstBody, *secondBody;
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask  )
    {
        
        int RockPos_X = Rock.position.x;
        int Rock2Pos_X = Rock2.position.x;
        NSLog(@" RockPos_X = %d ", RockPos_X);
        RockPos_X = 60 - RockPos_X;
        Rock2Pos_X = 260 - Rock2Pos_X;
        
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
        SKAction * moveToPositionRock = [SKAction sequence:@[[SKAction moveByX:RockPos_X y:0 duration: 1]]];
        SKAction * moveToPositionRock2 = [SKAction sequence:@[[SKAction moveByX:Rock2Pos_X y:0 duration: 1]]];
        [Rock runAction:moveToPositionRock];
        [Rock2 runAction:moveToPositionRock2];
        NSLog(@"CONTACT BETWEEN %@ %@", contact.bodyA.node.name ,contact.bodyB.node.name);
        NSLog(@"ROCK CHILDREN: %@",Rock.children);
        NSLog(@"ROCK2 CHILDREN: %@",Rock2.children);
        SKLabelNode *theLabel = [contact.bodyB.node.children firstObject];
        SKLabelNode *theHullText = [contact.bodyA.node.children firstObject];
        NSArray *resultSet = [self allNumbersFromString:theLabel.text];
        NSLog(@"ARRAY FROM LABEL %@" ,[self allNumbersFromString:theLabel.text]);
    
        NSInteger sumFromNodeB = [[resultSet firstObject] integerValue] + [[resultSet objectAtIndex:1] integerValue];
        NSInteger sumFromNodeA = [[[self allNumbersFromString:theHullText.text] firstObject]integerValue ];

        if(sumFromNodeA == sumFromNodeB)
        {
            CounterEROR = 1;
            [self BOOM : hull.position.x : hull.position.y+20];
            Score+=10;
            [scoreLable setText:[NSString stringWithFormat:@"Score %d", Score]];
            [self configureTwoRocks : CounterEROR];
            CounterEROR = 0;
        }
        else
        {
            CounterEROR = CounterEROR + 1;
            ++CounterEROR;
            [self configureTwoRocks : CounterEROR];
            NSLog(@"MISSED");
        }
    }
    else
    {
        ++i;
        if (i==1)
        {
            [self BOOM : Rock.position.x+1 : Rock.position.y-20];
            [self BOOM : Rock2.position.x : Rock2.position.y-20];
            firstBody = contact.bodyB;
            secondBody = contact.bodyA;
            [HitPoint setText:[NSString stringWithFormat:@"hp Earth %d ", hp = hp - 10]];
            if (hp==0) {
                GameOver *scene = [[GameOver alloc]initWithSize:self.size];
                scene.ScoreInGO =  [NSNumber numberWithInt:Score];
                SKTransition *doors = [SKTransition doorsOpenHorizontalWithDuration:0.2];
                [self.view presentScene:scene transition:doors];
            }
            [self configureTwoRocks:1];
        }
        else
        {
            i=0;
        };
    }
}

//=========================================================================

-(void) RemoveInvisibleMeteorites {

    [self enumerateChildNodesWithName:@"Rock" usingBlock:^(SKNode *node, BOOL *stop)
    {
            if (node.position.y < 0)
                [node removeFromParent];
    }];
}

//=========================================================================

-(void)configureTwoRocks : (int) b
{
    int firstX,secondX;
    firstX = rand()%130 + 30;
    secondX = rand()%140 + 10 + 150;
    int sum;
    switch (b) {
            case 1:
        {
            digit1 = rand() % 10;
            digit2 = rand() % 10;
            digit3 = rand() % 10;
            digit4 = rand() % 10;
            digit5 = rand() % 10;
            digit6 = rand() % 10;
            int a = rand() % 4;
            NSLog(@" a = %d ", a);
            if ( a <= 2 )
            {
                sum = digit1 + digit2;
            }
            else
            {
                sum = digit3 + digit4;
            }
            [self createFirstRock:digit1 :digit2 :firstX];
            [self createSecondRock:digit3 :digit4 :secondX];
            [self createHull :sum];
            break;
        }
            case 2:
        {
            [hull removeFromParent];
            int a = rand() % 3;
            NSLog(@" a = %d ", a);

            switch (a) {
                case 0:
                    sum = digit1 + digit2;
                    break;
                case 1:
                    sum = digit3 + digit4;
                    break;
                case 2:
                    sum = digit5 + digit6;
                    break;
                    
                default:
                    break;
            }
            
            float RockPos_Y = Rock.position.y;
            [self createThirdRock:digit5 :digit6 :RockPos_Y];
            [self createHull :sum];
            Score-=20;
            [scoreLable setText:[NSString stringWithFormat:@"Score %d", Score]];
            break;
 
        }
            case 4:
        {
            [self enumerateChildNodesWithName:@"Rock" usingBlock:^(SKNode *node, BOOL *stop) {
            
                [node removeFromParent];
            }];
            [hull removeFromParent];
            int a = rand() % 4;
            NSLog(@" a = %d ", a);
            if ( a <= 2 )
            {
                sum = digit1 + digit2;
            }
            else
            {
                sum = digit3 + digit4;
            }
            [self createFirstRock:digit1 :digit2 :firstX];
            [self createSecondRock:digit3 :digit4 :secondX];
            [self createHull :sum];
            CounterEROR = 0;
            [HitPoint setText:[NSString stringWithFormat:@"hp Earth %d ", hp = hp - 10]];
            if (hp==0) {
                GameOver *scene = [[GameOver alloc]initWithSize:self.size];
                scene.ScoreInGO =  [NSNumber numberWithInt:Score];
                SKTransition *doors = [SKTransition doorsOpenHorizontalWithDuration:0.2];
                [self.view presentScene:scene transition:doors];
            }
            break;
        }
    }
}

//=========================================================================

-(NSArray *)allNumbersFromString:(NSString *)str
{
    //NSString *originalString = @"(123) 123123 abc";
    NSMutableArray * resultSet = [NSMutableArray array];
    
    NSScanner *scanner = [NSScanner scannerWithString:str];
    NSCharacterSet *numbers = [NSCharacterSet
                               characterSetWithCharactersInString:@"0123456789"];
    
    while ([scanner isAtEnd] == NO) {
        NSString *buffer;
        if ([scanner scanCharactersFromSet:numbers intoString:&buffer]) {
            [resultSet addObject:buffer];
        }
        // --------- Add the following to get out of endless loop
        else {
            [scanner setScanLocation:([scanner scanLocation] + 1)];
        }    
        // --------- End of addition
    }
    return resultSet;
}

//=========================================================================

@end
