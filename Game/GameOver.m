//
//  GameOver.m
//  Game
//
//  Created by Admin on 28.10.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import "GameOver.h"

@implementation GameOver
@synthesize ScoreInGO;
-(void) didMoveToView:(SKView *)view
{
    if(!createScene)
    {
        [self removeAllActions];
        [self removeAllChildren];
        [self createBG];
        [self LabelGameOver];
        [self labelScore];
    }
}

//=========================================================================

-(void) createBG
{
    SKSpriteNode * bg = [SKSpriteNode spriteNodeWithImageNamed:@"GameOver"];
    bg.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    [self addChild:bg];
}

//=========================================================================

-(void) LabelGameOver
{
    SKLabelNode *LableGameOver = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    LableGameOver.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)+20);
    LableGameOver.fontSize = 30;
    LableGameOver.fontColor = [SKColor yellowColor];
    LableGameOver.text = [NSString stringWithFormat:@"Game Over"];
    [self addChild:LableGameOver];
}

//=========================================================================

-(void) labelScore
{
    SKLabelNode *LableScore = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    LableScore.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame)-20);
    LableScore.fontSize = 20;
    LableScore.fontColor = [SKColor yellowColor];
    LableScore.text = [NSString stringWithFormat:@" Score: %@", ScoreInGO];
    [self addChild:LableScore];
}

@end
