//
//  Struct.h
//  Game
//
//  Created by Admin on 03.09.14.
//  Copyright (c) 2014 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Struct : NSObject
{
    int _FirstNumber;
    int _SecondNumber;
    int _SumNumber;
}
/** Setter for properties.
 @param First  Sets the first property
 @param Second Second property
 @param Sum sum 
 @return
 */
-(void)setNumber:(int) First :(int) Second :(int) Sum;

@end
